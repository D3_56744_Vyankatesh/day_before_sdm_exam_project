create table emp(
    empid int PRIMARY KEY AUTO_INCREMENT,
    name varchar(200),
    salary double,
    age int
);

insert into emp(name, salary, age) values('vyankatesh kolhapure', 75000, 23);
insert into emp(name, salary, age) values('prakash mane', 175000, 39);
insert into emp(name, salary, age) values('mahesh kale', 95000, 44);
insert into emp(name, salary, age) values('uday reddy', 20000, 21);

