const express = require("express")
const db = require('./db')
const util = require('./utils')
const app = express()
app.use(express.json())

app.post('/add-emp', (request, response) => {
    console.log(request.body);
    const { name, salary, age } = request.body
    // const connection = db.openDataBaseConnection()
    const statement = `insert into emp(name, salary, age) values('${name}', ${salary}, ${age})`
    db.execute(statement, (error, result) => {
        // connection.end()
        response.send(util.createResult(error, result))
    })
})

app.get('/get-emps', (request, response) => {
    const statement = `select name, salary, age from emp`
    // const connection = db.openDataBaseConnection()
    db.execute(statement, (error, result) => {
        // connection.end()
        response.send(util.createResult(error, result))
    })
})

app.put('/update-emp/:id', (request, response) => {
    const { id } = request.params
    const { salary } = request.body

    const statement = `update emp set salary = ${salary} where empid = ${id}`
    db.execute(statement, (error, result) => {
        console.log(result);
        if (result['affectedRows'] == 0) {
            response.send(util.createResult('emp not found', null))
            return
        }
        response.send(util.createResult(error, result))
    })
})

app.delete('/delete-emp/:id', (request, response) => {
    const { id } = request.params
    const statment = `delete from emp where empid = ${id}`

    db.execute(statment, (error, result) => {
        if (result['affectedRows'] == 0) {
            response.send(util.createResult('emp not found', null))
            return
        }
        db.execute(statment, (error, result) => {
            response.send(util.createResult(error, result))
        })
    })
})

app.listen(4000, () => {
    console.log('server started at port 4000');
})